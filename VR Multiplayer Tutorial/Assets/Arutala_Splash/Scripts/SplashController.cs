﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using UnityEngine.UI;

public class SplashController : MonoBehaviour {

	public int levelToLoad = 1;

    [SerializeField]
    public float minimumTimeToShowLogo = 3f;

    [SerializeField]
    VideoPlayer videoSplash;

    [SerializeField]
    Material defaultMaterial;

    [SerializeField]
    Text versionText;

    [SerializeField]
    GameObject loadingCircle;

    [SerializeField]
    bool isReleaseBuild = false;



    IEnumerator Start () {

        //UnityEngine.XR.InputTracking.Recenter();

        //set version code to empty
        versionText.text = "";
		
		float minimumTimeEnd = Time.realtimeSinceStartup + minimumTimeToShowLogo;

        // intro
        StartCoroutine(PlayVideoSplash());

        // background load the new scene (but don't activate it yet)
        AsyncOperation o = SceneManager.LoadSceneAsync(levelToLoad);
		o.allowSceneActivation = false;
		while (o.isDone) {
			yield return new WaitForEndOfFrame();
		}

        // delay until minimum time is reached
        if (Time.realtimeSinceStartup < minimumTimeEnd)
        {
            yield return new WaitForSeconds(minimumTimeEnd - Time.realtimeSinceStartup);
        }

        StartCoroutine(ShowLoading());

        // outro
        yield return new WaitForSeconds(minimumTimeToShowLogo + 3f);

        // activate scene
        o.allowSceneActivation = true;
	}

    IEnumerator PlayVideoSplash()
    {
        // wait 1 second to change material to default to fix glitch in first frame
        yield return new WaitForSeconds(1f);
        videoSplash.GetComponent<Renderer>().material = defaultMaterial;
    }

    IEnumerator ShowLoading()
    {
        yield return new WaitForSeconds(minimumTimeToShowLogo);
        loadingCircle.SetActive(true);

        //get version code
        if (isReleaseBuild)
            versionText.text = "";
        else
            versionText.text = "version: " + Application.version;
        
    }
}
