﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class ContinuousMovement : MonoBehaviour
{
    //public XRNode inputSource;
    [Header("Character")]
    public List<XRController> controllers = null;
    public float speed = 0.5f;
    public float gravity = -9.81f;
    public LayerMask groundLayer;

    private float fallingSpeed;
    private XRRig rig;
    private Vector2 inputAxis;
    private CharacterController character;
    private Quaternion headYaw;
    private GameObject userCamera;

    // Start is called before the first frame update
    void Start()
    {
        character = GetComponent<CharacterController>();
        rig = GetComponent<XRRig>();
        userCamera = GameObject.FindGameObjectWithTag("MainCamera");
        //playerRotationState = PlayerRotationState.Forward;
    }

    private void FixedUpdate()
    {
        CapsuleFollowHeadset();
        CheckInputToMove();
        //if (GeneralManager.instance.isStarted)
        //{
        //    CheckInputToMove();
        //}
    }

    private void CapsuleFollowHeadset()
    {
        character.height = rig.cameraInRigSpaceHeight + 0.02f;
        Vector3 capsuleCenter = transform.InverseTransformPoint(rig.cameraGameObject.transform.position);
        character.center = new Vector3(capsuleCenter.x, character.height / 2 + character.skinWidth, capsuleCenter.z);
    }

    private bool CheckIfGrounded()
    {
        //tells us if on ground
        Vector3 rayStart = transform.TransformPoint(character.center);
        float rayLength = character.center.y + 0.01f;
        bool hasHit = Physics.SphereCast(rayStart, character.radius, Vector3.down, out RaycastHit hitInfo, rayLength, groundLayer);
        return hasHit;
    }

    private void CheckInputToMove()
    {
        foreach(XRController controller in controllers)
        {
            if (controller.enableInputActions)
                CheckForMovement(controller.inputDevice);
        }
    }

    private void CheckForMovement(InputDevice device)
    {
        if (device.TryGetFeatureValue(CommonUsages.primary2DAxis, out inputAxis))
            StartMove();
    }

    private void StartMove()
    {
        headYaw = Quaternion.Euler(0, rig.cameraGameObject.transform.eulerAngles.y, 0);
        Vector3 direction = headYaw * new Vector3(0, 0, inputAxis.y);
        //Vector3 direction = new Vector3(0, 0, inputAxis.y);
        //Vector3 direction = headYaw * new Vector3(inputAxis.x, 0, inputAxis.y);

        if (inputAxis.y < 0)
        {
            speed = 0.25f;
            //character.Move(direction * Time.fixedDeltaTime * speed);
        }
        else if(inputAxis.y > 0)
        {
            speed = 0.5f;
        }

        character.Move(direction * Time.fixedDeltaTime * speed);

        //gravity
        bool isGrounded = CheckIfGrounded();
        if (isGrounded)
            fallingSpeed = 0;
        else
            fallingSpeed += gravity * Time.fixedDeltaTime * speed;

       character.Move(Vector3.up * fallingSpeed * Time.fixedDeltaTime);
    }
}
